@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Edit User</div>

                    <div class="panel-body">
                        {!! Form::model($user, ['route' => ['users.update', $user->id], "method" => "PATCH"]) !!}
                        <div class="form-group">
                            <label for="name">Name:</label>
                            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
                        </div>

                        <div class="form-group">
                            <label for="email">Email address:</label>
                            {{ Form::text('email', null,
                                ['class' => 'form-control', 'id' => 'email'])
                            }}
                        </div>

                        <div class="form-group">
                            <label for="profile">Profile:</label>
                            {{ Form::select('profile',
                                ['' => 'Select', 'admin' => 'Admin', 'user' => 'User'], null,
                                ["class" => "form-control", "id" => "profile"])
                            }}
                        </div>

                        <div class="form-group">
                            {{ Form::submit('Update', ["class" => "btn btn-info"]) }}
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection