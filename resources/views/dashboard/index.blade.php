@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Assigned Tasks -
                        <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-info btn-xs"
                           id="create_task">Create Task</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Reporter</th>
                                <th>Priority</th>
                                <th>Due Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tasks as $task)
                                <tr>
                                    <td>{{ $task->name }}</td>
                                    <td>{{ $task->reporter->name }}</td>
                                    <td>{{ $task->priority }}</td>
                                    <td>{{ $task->due_date }}</td>
                                    <td>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Reported Tasks
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Assigned</th>
                                <th>Priority</th>
                                <th>Due Date</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reportedTasks as $reportedTask)
                                <tr>
                                    <td>{{ $reportedTask->name }}</td>
                                    <td>{{ $reportedTask->user->name }}</td>
                                    <td>{{ $reportedTask->priority }}</td>
                                    <td>{{ $reportedTask->due_date }}</td>
                                    <td>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">New Task</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => 'tasks.store', "method" => "POST"]) !!}
                    <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">

                    <div class="form-group">
                        <label for="user_id">Assign To:</label>
                        {{ Form::select('user_id',
                            $users, null,
                            ["class" => "form-control", "id" => "priority"])
                        }}
                    </div>

                    <div class="form-group">
                        <label for="name">Name:</label>
                        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        {{ Form::textarea('description', null,
                            ['class' => 'form-control', 'id' => 'description'])
                        }}
                    </div>

                    <div class="form-group">
                        <label for="priority">Priority:</label>
                        {{ Form::select('priority',
                            ['' => 'Select', 'hight' => 'Hight', 'average' => 'Average', 'low' => 'Low'], null,
                            ["class" => "form-control", "id" => "priority"])
                        }}
                    </div>

                    <div class="form-group">
                        <label for="due_date">Due Date:</label>
                        {{ Form::text('due_date', null, ['class' => 'form-control datepicker', 'id' => 'due_date']) }}
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create Task</button>
                    </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
@endsection