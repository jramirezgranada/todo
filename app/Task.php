<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ["name", "description", "priority", "user_id", "due_date", "created_by"];

    /**
     * Get the assigned user to a task
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function reporter()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

}
