<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->tasks = Auth::user()->tasks;
            $this->user = Auth::user();
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $tasks = $this->tasks;

        $users = User::pluck('name', 'id');

        $reportedTasks = Task::where('created_by', $this->user->id)->get();

        return view('dashboard.index')->with(compact('tasks', 'users', 'reportedTasks'));
    }

}
