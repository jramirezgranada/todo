<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class UsersController extends DashboardController
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * List all Users
     *
     * @return $users
     */
    public function index(){
        $users = User::all();

        return view('admin.users.index')->with(compact('users'));
    }

    /**
     * Show form to edit an user
     *
     * @param $id
     * @return $user
     */
    public function edit($id){
        $user = User::findOrFail($id);

        return view('admin.users.edit')->with(compact('user'));
    }

    /**
     * Delete an user from database
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $user = User::findOrFail($request->get('id'));
        $user->destroy($request->get('id'));

        Session::flash('message', 'User Deleted !!');

        return redirect()->route('users.index');
    }


    /**
     * Show form to create an user
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        return view('admin.users.create');
    }

    /**
     * Save a new user
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|required|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'profile' => 'required',
            'password_confirmation' => 'required|min:6',
        ]);

        $user = new User();
        $user->fill($request->all());
        $user->save();

        Session::flash('message', "User created successfully");

        return redirect()->route('users.index');
    }

    /**
     * Update an user
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|required|unique:users,email',
            'profile' => 'required'
        ]);

        $user = User::findOrFail($id);

        $user->fill($request->except('_token', '_method'));
        $user->save();

        Session::flash('message', "User Updated");

        return redirect()->route('users.index');
    }
}
