<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {

    }

    public function store(Request $request)
    {
        $task = Task::create($request->all());

        return redirect()->route('home');
    }
}
