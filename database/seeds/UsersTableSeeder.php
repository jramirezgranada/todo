<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Admin";
        $user->email = "admin@admin.com";
        $user->password = bcrypt("admin");
        $user->profile = "admin";
        $user->save();

        $user = new User();
        $user->name = "User";
        $user->email = "user@user.com";
        $user->password = bcrypt("user");
        $user->profile = "user";
        $user->save();
    }
}
