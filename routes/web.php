<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin', "middleware" => "auth"], function(){
    Route::get('/', 'Admin\DashboardController@index')->name('home');
    Route::get('/users', 'Admin\UsersController@index')->name('users.index');
    Route::get('/users/create', 'Admin\UsersController@create')->name('users.create');
    Route::post('/users/create', 'Admin\UsersController@store')->name('users.store');
    Route::get('/users/edit/{id}', 'Admin\UsersController@edit')->name('users.edit');
    Route::patch('/users/edit/{id}', 'Admin\UsersController@update')->name('users.update');
    Route::delete('/users/delete', 'Admin\UsersController@destroy')->name('users.destroy');

});

Route::group(["prefix" => "dashboard", "middleware" => "auth"], function(){
    Route::get('/', 'DashboardController@index')->name('home');
    Route::post('tasks/store', 'TaskController@store')->name('tasks.store');
});
