# ToDo Test

### Requirements
* PHP 7.*
* Mod Rewrite Enabled
* Mcrypt Installed
* Composer Installed Globally

### Setup Project
* Clone the project https://bitbucket.org/jramirezgranada/todo
* CD to project folder 
* Run composer install
* php artisan key:generate
* php artisan migrate:install
* php artisan migrate
* php artisan db:seed to create some initial users
* There are 2 kinds of users, admin and user.
* admin@admin.com -- password: admin
* user@user.com -- password: user

### Description
* There are two areas in the project, the first one is only accesibe by admin user, in that area 
the admin can create, edit, delete users, and create tasks to other users.
* A normal user only can create, edit or delete tasks, in the dashboard view we have 2 tables,
the first one is my assigned tickets and the second one is the tickets created by the logged user.
 
 
